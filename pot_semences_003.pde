import wblut.math.*;
import wblut.processing.*;
import wblut.core.*;
import wblut.hemesh.*;
import wblut.geom.*;
import java.util.Iterator;

HE_Mesh mesh;
WB_Render render;
int zoomzoom = 0;

void setup() {
  size(800, 800, OPENGL);
  // la forme du pot
  HEC_Cylinder creator=new HEC_Cylinder();
  creator.setRadius(30, 20); // upper and lower radius. If one is 0, HEC_Cone is called.
  creator.setHeight(70);
  creator.setFacets(9).setSteps(6);
  creator.setCap(true, true);// cap top, cap bottom?
  creator.setZAxis(0, 1, 1);

  mesh=new HE_Mesh(creator); 
  HET_Diagnosis.validate(mesh);

  // créer des subdivisions aléatoires
  subdivideMesh();
  modifyMesh();

  // create a lattice (des parois creuses)
  //HEM_Lattice lat = new HEM_Lattice().setWidth( 6 ).setDepth( 2 );
  // mesh.modify( lat );

  // modify the mesh, arrondi les angles
  HES_CatmullClark cc = new HES_CatmullClark();
  mesh.subdivide( cc, 2 );

  HET_Export.saveToSTL( mesh, sketchPath("mymodel.stl"), 0.1 );

  render=new WB_Render(this);
}

void draw() {
  background(255);
  directionalLight(255, 255, 255, 1, 1, -1);
  directionalLight(127, 127, 127, -1, -1, 1);

  // dessin de la forme à l'aide de transformations de matrices pour
  // associer les 2 formes
  pushMatrix();
  translate(400, 400, 100);
  rotateY(mouseX*1.0f/width*TWO_PI);
  rotateX(mouseY*1.0f/height*TWO_PI);
  zoom();
  scale(zoomzoom);
  stroke(0);
  //render.drawEdges(mesh);
  noStroke();
  render.drawFaces(mesh);
  popMatrix();
}

void zoom() {
  if (mousePressed) {
    zoomzoom = int(mouseX*10.0f/width);
  }
}

