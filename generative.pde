void subdivideMesh()
{
  HE_Selection selection = new HE_Selection( mesh );

  HE_Face f;
  Iterator<HE_Face> fItr = mesh.fItr();
  while ( fItr.hasNext () ) {
    f = fItr.next();
    if ( f.getFaceOrder() < 6) {
      selection.add( f );
      //println("il se passe quelque chose");
    }
  }

  HES_Planar planar = new HES_Planar();
  planar.setRandom( true );
  planar.setRange(0.7);

  // modify the mesh
  mesh.subdivideSelected( planar, selection );
}

void modifyMesh()
{
  HE_Selection selection = new HE_Selection( mesh );

  HE_Face f;
  Iterator<HE_Face> fItr = mesh.fItr();
  int counter = 0;
  while ( fItr.hasNext () ) {
    f = fItr.next();
    if ( f.getFaceOrder() < 6) {
      selection.add( f );
      //println("il se passe quelque chose");
    } 
    else {
      if (counter < 1) {
        selection.add(f);
        counter++;
      }
    }
  }

  // créer la latice (gruyere)
  HEM_Lattice lat = new HEM_Lattice().setWidth( 3 ).setDepth( 2 );
  mesh.modifySelected(lat, selection );

  //  // create the extruder
  //  HEM_Extrude ext = new HEM_Extrude();
  //  ext.setDistance( 20 ).setChamfer( 0.5 );
  //
  //  // modify the mesh
  //  mesh.modifySelected( ext, selection );
}

